import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Admin from './pages/Admin';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';

import './App.css';
import { UserProvider } from './UserContext';


function App() {

    // State hook for the user state that's defined here for a global scope
    // Initialized as an object with properties from the localStorage
    // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
    const [user, setUser] = useState({ id : null, isAdmin : null });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {

        if(localStorage.getItem('token')){
            fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
                headers : {
                    Authorization : `Bearer ${ localStorage.getItem('token') }`
                }
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                // if(data._id){
                    setUser({
                        id: data._id,
                        isAdmin : data.isAdmin
                    })
                // }
            })
        };

        console.log(user);
        console.log(localStorage);
    }, []);

    return (

    // - Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop
    <UserProvider value={{ user, setUser, unsetUser }}>
        {/*// - The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.

        // - The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint. For example, when the `/products` is visited in the web browser, React.js will show the `Products` component to us.*/}
        <Router>      
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/admin" element={<Admin />} />
                    <Route path="/products" element={<Products />} />
                    <Route path="/products-add" element={<AddProduct />} />
                    <Route path="/products/:productId" element={<ProductView />} />
                    <Route path="/products/:productId/update" element={<UpdateProduct />} />                     
                    <Route path="/login" element={<Login />} />
                    <Route path="/logout" element={<Logout />} /> 
                    <Route path="/register" element={<Register />} />      
                    <Route path="/*" element={<Error />} />     
                </Routes>
            </Container>
        </Router>
    </UserProvider>
      
  );
}

export default App;
