import { useState, useEffect, useContext } from 'react';
import { Container, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function UpdateProduct() {

	const { productId } = useParams();

	const { user } =  useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);


	function changeProductDetails(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
			method : "PUT",
		    headers : {
		        Authorization : `Bearer ${ localStorage.getItem('token') }`,
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        name: name,
		        description: description,
		        price: price
		    })
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

				if (data) {

				    Swal.fire({
				        title: 'Success',
				        icon: 'Succesfully updated product',
				        text: 'You have succesfully updated this product.'

				    });

				    navigate("/admin");

				} else {

				    Swal.fire({
				        title: 'Something went wrong',
				        icon: 'error',
				        text: 'Please try again.'   
				    });

				}
		})
	};

	useEffect(() => {
		// Validation to enable submit button when one of the fields is populated
		if(name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price]);

	return (

		<Form onSubmit={(e) => changeProductDetails(e)}>
		    <Form.Group controlId="name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Change name of product"
		            value={name} 
		            onChange={e => setName(e.target.value)}
		            require
		        />
		    </Form.Group>
		   	<Form.Group controlId="description">
		        <Form.Label>Description</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Change description of product"
		            value={description} 
		            onChange={e => setDescription(e.target.value)}
		            required
		        />
		    </Form.Group>
		    <Form.Group controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control 
		            type= "text"
		            placeholder="Change price of product"
		            value={price} 
		            onChange={e => setPrice(e.target.value)}
		            required
		        />
		    </Form.Group>
		    {
		    	isActive ?

		    		<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Apply</Button>
		    		:
		    		<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Apply</Button>
		    }
		</Form>

	)
}