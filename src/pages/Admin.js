import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AllProducts from '../components/AllProducts';

import UserContext from '../UserContext';

export default function Admin() {

	const { user, setUser } = useContext(UserContext)

	const [products, setProducts] = useState([]);

	useEffect(() => {
		if(localStorage.getItem('token')){
			fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			    headers : {
			        Authorization : `Bearer ${ localStorage.getItem('token') }`
			    }
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setProducts(data.map(product => {
					return (
						<AllProducts key={ product._id } product={ product } />
					)
				}))
			})
		}
	}, []);

	return (
		<Container className="mt-5" align="center">
            <h1>Admin Dashboard</h1>
            <Link variant="primary" type="submit" id="submitBtn" className="m-2" display="block" to={`/products-add`}>Add a new product</Link>
            <>
            	{ products }
            </>
        </Container>
	)

}
