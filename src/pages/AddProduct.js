import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {

	// State that will be used to store the products retrieved from the database
	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	function createProduct(e) {

		e.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
				method : "POST",
			    headers : {
			        Authorization : `Bearer ${ localStorage.getItem('token') }`,
			        'Content-Type': 'application/json'
			    },
			    body: JSON.stringify({
			        name: name,
			        description: description,
			        price: price
			    })
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

					if (data) {

					    Swal.fire({
					        title: 'Success',
					        icon: 'Succesfully added new product',
					        text: 'You have succesfully added a product.'

					    });

					navigate("/admin");

					} else {

					    Swal.fire({
					        title: 'Something went wrong',
					        icon: 'error',
					        text: 'Please try again.'   
					    });

					}
			})
	};

	useEffect(() => {
		// Validation to enable submit button when all fields are populated
		if(name !== '' && description !== '' && price !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [name, description, price]);

	return (

		<Form onSubmit={(e) => createProduct(e)}>
		    <Form.Group controlId="name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Enter name of product"
		            value={name} 
		            onChange={e => setName(e.target.value)}
		            required
		        />
		    </Form.Group>
		    <Form.Group controlId="description">
		        <Form.Label>Description</Form.Label>
		        <Form.Control 
		            type="text" 
		            placeholder="Enter description of product"
		            value={description} 
		            onChange={e => setDescription(e.target.value)}
		            required
		        />
		    </Form.Group>
		    <Form.Group controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control 
		            type= "text"
		            placeholder="Enter price of product"
		            value={price} 
		            onChange={e => setPrice(e.target.value)}
		            required
		        />
		    </Form.Group>
		    {
		    	isActive ?
		    		<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Create</Button>
		    		:
		    		<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Create</Button>
		    }
		</Form>
	)
}
