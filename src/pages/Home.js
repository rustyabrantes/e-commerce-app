import Banner from '../components/Banner';

export default function Home() {

    const data = {
        title: "RusTech Store",
        content: "Your ultimate destination for cutting-edge gadgets and electronics!",
        destination: "/products",
        label: "Shop Now!"
    }

    return (
        <>
            <Banner data={ data }/>
        </>
    )
}