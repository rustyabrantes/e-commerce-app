import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { productId } = useParams();

	const { user } =  useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [price, setPrice] = useState(0);

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
			method : "POST",
			headers : {
				"Content-Type" : "application/json",
				Authorization : `Bearer ${ localStorage.getItem('token') }`
			},
			body : JSON.stringify({
				productId : productId,
				productName : name,
				quantity : quantity,
				totalAmount : quantity*price
			})
		})
		.then(res => res.json())
		.then(data => {

			// console.log(data);

			if(data === true){

				Swal.fire({
					icon : 'success',
					title : 'Succesfully ordered product',
					text: "You have succesfully ordered this product."
				});

				navigate("/products");
			} else {

				Swal.fire({
					icon : 'error',
					title : 'Something went wrong',
					text: "Please try again."
				});
			}
		})
	};

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);

			})

	}, [productId]);

	return(
		<Container>
			<Row>
				<Col>
					<Card className="col-lg-6 m-auto mt-5">
						<Card.Body>
							<Card.Title>{ name }</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{ description }</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php { price*quantity }</Card.Text>
							<Card.Subtitle>Quantity:</Card.Subtitle>
							<Form.Group controlId="quantity">
							    <Form.Control 
							        type= "number"
							        value={quantity} 
							        onChange={e => setQuantity(e.target.value)}
							        required
							    />
							</Form.Group>
							{
								(user.id) ?
									<Button variant="primary" onClick={() => order(productId)}>Order</Button>
								:
									<Link className="btn btn-danger btn-block" to="/login">Log in to order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
