import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ product }) {

    // Checks to see if the data was successfully passed
    console.log(product);
    // Every component receives information in a form of an object
    console.log(typeof product);

    // Destructure the course prop from parent component Products.js
    const { _id, name, description, price } = product;

    return (
        <Card className="col-sm-12 col-md-6 col-lg-4 m-auto mt-5" align="center" style={{display: "inline-block"}}>
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP { price }</Card.Text>
                <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>           
            </Card.Body>
        </Card>
    )
}
