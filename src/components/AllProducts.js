// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AllProducts({ product }) {

    // Checks to see if the data was successfully passed
    console.log(product);
    // Every component receives information in a form of an object
    console.log(typeof product);

    const { productId } = useParams();

    // Destructure the course prop from parent component Products.js
    const { _id, name, description, price, isActive } = product;

    function activateProduct(e, productId) {

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
            method : "PATCH",
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    isActive : "true"
                })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

                if (data) {

                    Swal.fire({
                        title: 'You have activated this product!',
                        icon: 'success',
                        text: 'Success!'

                    });

                } else {

                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.'   
                    });

                }

            window.location.reload()
        })
    };

    function archiveProduct(e, productId) {

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method : "PATCH",
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    isActive : "false"
                })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

                if (data) {

                    Swal.fire({
                        title: 'You have archived this product!',
                        icon: 'success',
                        text: 'Success!'

                    });

                } else {

                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.'   
                    });

                }

            window.location.reload()
        })
    };

    return (
            <Card className="col-sm-12 col-md-6 col-lg-4 mt-3 m-1">
                <Card.Body>
                    <Card.Title>{ name }</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{ description }</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP { price }</Card.Text>
                    <Card.Subtitle>Product Availability:</Card.Subtitle>
                    <Card.Text> { `${isActive}` }</Card.Text>
                    <Link className="primary" to={`/products/${_id}/update`}>Update</Link>
                    <div>
                    {
                        (isActive) ?

                            <Button onClick={(e, productId) => activateProduct(e, productId)} variant="info" type="submit" id="submitBtn" disabled>Activate</Button>
                        :

                            <Button onClick={(e, productId) => activateProduct(e, productId)} variant="info" type="submit" id="submitBtn">Activate</Button>
                    }
                    {
                        (isActive === false) ?

                            <Button onClick={(e, productId) => archiveProduct(e, productId)} variant="danger" type="submit" id="submitBtn" disabled>Archive</Button>
                        :

                            <Button onClick={(e, productId) => archiveProduct(e, productId)} variant="danger" type="submit" id="submitBtn">Archive</Button>
                    }
                    </div>         
                </Card.Body>
            </Card>
    )
}
